import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";

import "../login-signup.css";
import { LOGIN_ACTION } from "../actions/type";
import { loginReducer } from "../reducers/loginReducer";
import {
  FormHeader,
  FormOptions,
  Input,
  Submit,
  Form,
} from "../styledComponents/formStyledComponent";

export default function Login() {
  const dispatch = useDispatch(loginReducer);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const data = { email, password };

  const onSubmit = (e) => {
    e.preventDefault();
    dispatch({ type: LOGIN_ACTION, payload: { data } });
  };

  return (
    <div>
      <Form id="login" onSubmit={onSubmit}>
        <FormHeader>Login To Your Account</FormHeader>
        <Input
          type="email"
          value={email}
          placeholder="Enter Email Address"
          onChange={(e) => setEmail(e.target.value)}
        />
        <Input
          type="password"
          value={password}
          placeholder="Password"
          onChange={(e) => setPassword(e.target.value)}
        />
        <FormOptions>
          <div id="signup-prompt">
            <Link to="/signup">Don't have an account?</Link>
          </div>
          <div id="forgot-password">
            <Link to="/forgot-password">Forgot Password?</Link>
          </div>
        </FormOptions>
        <Submit type="submit" value="Login" />
      </Form>
    </div>
  );
}
